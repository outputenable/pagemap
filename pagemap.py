#!/usr/bin/env python3
#
# Copyright (C) 2017, 2018, 2020 Oliver Ebert <oe@outputenable.net>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name of an author or copyright
# holder shall not be used in advertising or otherwise to promote the
# sale, use or other dealings in this Software without prior written
# authorization from the author or copyright holder.
#

"""This module provides various utilities for the Linux
``/proc/$PID/pagemap`` and related interfaces.

* man 5 proc
* https://www.kernel.org/doc/Documentation/vm/pagemap.txt
* https://www.kernel.org/doc/Documentation/vm/soft-dirty.txt
"""

from array import array
import os
from pathlib import Path
import re
from typing import BinaryIO, Iterable, Optional, Sequence, Tuple


class AddressRange:
    """A (virtual) address range."""

    __slots__ = "bounds"

    def __init__(self, start: int, end: Optional[int] = None):
        """Create a (virtual) address range.

        :param start: The start address.
        :param end: The end address (exclusive!).  If ``None`` then
            ``start + 1`` is assumed.
        """
        self.bounds = start, end if end is not None else start + 1

    @property
    def start(self) -> int:
        """Get the range's start address."""
        return self.bounds[0]

    @property
    def end(self) -> int:
        """Get the range's end address."""
        return self.bounds[1]

    def size(self) -> int:
        """Return the size of this range.

        :returns: The size of this range.
        """
        return max(0, self.end - self.start)

    def empty(self) -> bool:
        """Return whether this range is empty.

        :returns: ``True`` if this range is empty (that is, has a size
            of zero), ``False`` otherwise.
        """
        return self.start >= self.end

    def contains(self, start: int, end: Optional[int] = None) -> bool:
        """Return whether this range contains a given (virtual) address
        range.

        :param start: The start address.
        :param end: The end address (exclusive!).  If ``None`` then
            ``start + 1`` is assumed.
        :returns: ``True`` if this range contains all of the range given
            by ``start`` and ``end``, ``False`` otherwise.
        """
        return (
            self.start <= start < self.end
            if end is None
            else self.start <= start <= end <= self.end
        )

    def __str__(self) -> str:
        return "{:0=8x}-{:0=8x}".format(self.start, self.end)


class Mapping(AddressRange):

    __slots__ = (
        "vm_start",
        "vm_end",
        "perms",
        "offset",
        "dev_major",
        "dev_minor",
        "inode",
        "pathname",
    )

    READ    = "r"
    WRITE   = "w"
    EXECUTE = "x"
    SHARED  = "s"
    PRIVATE = "p"

    @staticmethod
    def _from_maps_line_match(match: re.Match) -> "Mapping":
        return Mapping(
            int(match[1], 16),  # start address
            int(match[2], 16),  # end address
            match[3],  # permissions
            int(match[4], 16),  # offset
            int(match[5], 16),  # device major
            int(match[6], 16),  # device minor
            int(match[7]),  # inode
            match[8],  # pathname
        )

    def __init__(
        self,
        vm_start: int,
        vm_end: int,  # exclusive
        perms: str,
        offset: int,
        dev_major: int,
        dev_minor: int,
        inode: int,
        pathname: str,
    ):
        super().__init__(vm_start, vm_end)
        self.perms = perms
        self.offset = offset
        self.dev_major = dev_major
        self.dev_minor = dev_minor
        self.inode = inode
        self.pathname = pathname

    def __str__(self) -> str:
        # mimic the format of /proc/$PID/maps
        s = "{} {} {:0=8x} {:0=2x}:{:0=2x} ".format(
            super().__str__(),
            self.perms,
            self.offset,
            self.dev_major,
            self.dev_minor
        )
        return "{}{:<{:d}d} {}".format(
            s,
            self.inode,
            max(0, 72 - len(s)),  # width for inode
            self.pathname
        )


# Regular expression to match a line in /proc/$PID/maps.
_MAPS_LINE_REGEX = re.compile(
    r"""\s*([0-9A-Fa-f]+)-([0-9A-Fa-f]+)  # virtual address [start-end)
        \s+(\S+)                          # permissions
        \s+([0-9A-Fa-f]+)                 # offset
        \s+([0-9A-Fa-f]+):([0-9A-Fa-f]+)  # device major:minor
        \s+([0-9]+)                       # inode
        \s*(.*)                           # pathname""",
    re.VERBOSE,
)


def maps_path_for_pid(pid: int) -> Path:
    return Path("/proc/{:d}/maps".format(pid))


def mappings(pid: int) -> Iterable[Mapping]:
    with maps_path_for_pid(pid).open() as maps:
        lines = maps.readlines()
    return (
        Mapping._from_maps_line_match(_MAPS_LINE_REGEX.match(line)) for line in lines
    )


class Page:

    __slots__ = "value"

    PRESENT             = 1 << 63
    SWAPPED             = 1 << 62
    FILE_OR_SHARED_ANON = 1 << 61
    MAPPED_EXCLUSIVELY  = 1 << 56
    SOFT_DIRTY          = 1 << 55

    PFN_MASK = (1 << 55) - 1

    SWAP_OFFSET_SHIFT = 5
    SWAP_OFFSET_MASK  = (1 << 50) - 1
    SWAP_TYPE_MASK    = (1 << 5) - 1

    def __init__(self, value: int = 0):
        self.value = value

    def is_present(self) -> bool:
        return (self.value & Page.PRESENT) != 0

    def is_swapped(self) -> bool:
        return (self.value & Page.SWAPPED) != 0

    def is_file_or_shared_anon(self) -> bool:
        return (self.value & Page.FILE_OR_SHARED_ANON) != 0

    def is_mapped_exclusively(self) -> bool:
        return (self.value & Page.MAPPED_EXCLUSIVELY) != 0

    def is_soft_dirty(self) -> bool:
        return (self.value & Page.SOFT_DIRTY) != 0

    def get_pfn(self) -> int:
        return self.value & Page.PFN_MASK

    def get_swap_type(self) -> int:
        return self.value & Page.SWAP_TYPE_MASK

    def get_swap_offset(self) -> int:
        return (self.value >> Page.SWAP_OFFSET_SHIFT) & Page.SWAP_OFFSET_MASK

    def __str__(self) -> str:
        if not self.is_swapped():
            s = "{}{:0=8x}".format(
                "PFN:" if self.is_present() else "    ",
                self.get_pfn()
            )
        else:
            s = "SWP:{:2d}+{:0=8x}".format(
                self.get_swap_type(),
                self.get_swap_offset()
            )
        return "{:<15s} shared:{:d} excl:{:d} soft-dirty:{:d}".format(
            s,
            self.is_file_or_shared_anon(),
            self.is_mapped_exclusively(),
            self.is_soft_dirty(),
        )


PAGE_SIZE = os.sysconf("SC_PAGESIZE")
PAGEMAP_ENTRY_SIZE = 8


def pagemap_path_for_pid(pid: int) -> Path:
    return Path("/proc/{:d}/pagemap".format(pid))


def virt_to_page(start: int, end: Optional[int] = None) -> Tuple[int, int]:
    if end is None:
        end = start + 1
    return start // PAGE_SIZE, (end + (PAGE_SIZE - 1)) // PAGE_SIZE


def read_pages(
    pagemap: BinaryIO, pg_start: int, pg_end: Optional[int] = None
) -> Sequence[Page]:
    if pg_end is None:
        pg_end = pg_start + 1
    pagemap.seek(pg_start * PAGEMAP_ENTRY_SIZE)
    values = array("Q")  # unsigned long long
    values.fromfile(pagemap, pg_end - pg_start)
    return [Page(value) for value in values]


def pagemap(
    pid: int, vms: Iterable[AddressRange]
) -> Sequence[Tuple[AddressRange, Sequence[Page]]]:
    with pagemap_path_for_pid(pid).open("rb", buffering=0) as f:
        return [(vm, read_pages(f, *virt_to_page(*vm.bounds))) for vm in vms]


KPF_LOCKED        = 1 << 0
KPF_ERROR         = 1 << 1
KPF_REFERENCED    = 1 << 2
KPF_UPTODATE      = 1 << 3
KPF_DIRTY         = 1 << 4
KPF_LRU           = 1 << 5
KPF_ACTIVE        = 1 << 6
KPF_SLAB          = 1 << 7
KPF_WRITEBACK     = 1 << 8
KPF_RECLAIM       = 1 << 9
KPF_BUDDY         = 1 << 10
KPF_MMAP          = 1 << 11
KPF_ANON          = 1 << 12
KPF_SWAPCACHE     = 1 << 13
KPF_SWAPBACKED    = 1 << 14
KPF_COMPOUND_HEAD = 1 << 15
KPF_COMPOUND_TAIL = 1 << 16
KPF_HUGE          = 1 << 17
KPF_UNEVICTABLE   = 1 << 18
KPF_HWPOISON      = 1 << 19
KPF_NOPAGE        = 1 << 20
KPF_KSM           = 1 << 21
KPF_THP           = 1 << 22
KPF_BALLOON       = 1 << 23
KPF_ZERO_PAGE     = 1 << 24
KPF_IDLE          = 1 << 25

KPAGE_ENTRY_SIZE = 8


def read_kpage(file, pfns, default=0):
    values = array("Q")  # unsigned long long
    for pfn in pfns:
        if pfn is not None:
            file.seek(pfn * KPAGE_ENTRY_SIZE)
            values.fromfile(file, 1)
        else:
            values.append(default)
    return values


def open_kpagecount():
    return open("/proc/kpagecount", "rb", buffering=0)


def open_kpageflags():
    return open("/proc/kpageflags", "rb", buffering=0)


def open_kpagecgroup():
    return open("/proc/kpagecgroup", "rb", buffering=0)


def main():
    import argparse
    from argparse import ArgumentError, ArgumentParser

    class AddressRangeAction(argparse.Action):
        def __init__(self, option_strings, dest, nargs=None, **kwargs):
            super().__init__(option_strings, dest, nargs, **kwargs)

        def __call__(self, parser, namespace, values, option_string=None):
            ranges = getattr(namespace, self.dest, [])
            for value in values:
                bounds = value.split("-")
                if len(bounds) not in (1, 2) or "" in bounds:
                    raise ArgumentError(
                        self, "Invalid address range (expected start[-end]): " + value
                    )
                for i in range(len(bounds)):
                    try:
                        bounds[i] = int(bounds[i], 0)
                    except ValueError:
                        raise ArgumentError(self, "Invalid address: " + bounds[i])
                addr = AddressRange(*bounds)
                if addr.empty():
                    raise ArgumentError(self, "Address range is empty: " + value)
                ranges.append(addr)
            setattr(namespace, self.dest, ranges)

    parser = ArgumentParser()
    parser.add_argument("pid", type=int, metavar="PID", help="Process ID.")
    parser.add_argument(
        "ranges",
        nargs="*",
        action=AddressRangeAction,
        metavar="ADDR",
        help="Virtual address (range): start[-end].",
    )

    args = parser.parse_args()

    entries = pagemap(args.pid, args.ranges or mappings(args.pid))
    for vm, pages in entries:
        print(vm)
        for p in pages:
            print(" ", p)


if __name__ == "__main__":
    main()
